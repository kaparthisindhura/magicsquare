import java.util.Scanner;

import static java.lang.Integer.*;

public class MagicSquare {
    public static void main(String[] args) {
        int n,j,i,sum=0,rowSum,colSum,forDiaSum=0,backDiaSum=0,flag=0;
        Scanner sc=new Scanner(System.in);
        n= parseInt(sc.nextLine());
        int[][] mat = new int[n][n];
        for(i=0;i<n;i++)
        {
            j=0;
            for (String s1 :  sc.nextLine().split(" "))
            {
                mat[i][j] = parseInt(s1);
                j++;
            }
        }
        for(i=0;i<n;i++)
        {
            rowSum=0;
            colSum=0;
            for(j=0;j<n;j++)
            {
                rowSum += mat[i][j];
                colSum += mat[j][i];
                if(i==j)
                {
                    forDiaSum += mat[i][j];
                }
                if((i+j)==(n-1))
                {
                    backDiaSum += mat[i][j];
                }
            }
            if(flag==1)
            {
                if((sum!=rowSum) || (sum!=colSum))
                {
                    System.out.println("No");
                    return ;
                }
            }
            else if( rowSum==colSum)
            {
                sum=rowSum;
                flag=1;
            }
            else
            {
                System.out.println("No");
                return;
            }

        }

        if((sum!=forDiaSum) || (sum!=backDiaSum) )
        {
            System.out.println("No");
            return ;
        }
        System.out.println("Yes");


    }
}
